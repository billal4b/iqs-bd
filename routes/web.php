<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/clear-cache', function() {
//     Artisan::call('cache:clear');
//     return "Cache is cleared";
// });

Route::get('/', 'Frontend\FrontendHomeController@index');
Route::get('/courses', 'Frontend\FrontendHomeController@courses');

Route::get('/registration','Auth\RegistrationController@registerForm')->name('register.form');
Route::post('/registration','Auth\RegistrationController@registerCreate')->name('register.create');

Route::get('/quran-teaching-course', 'Frontend\FrontendCoursesController@quranTeachingCourse');
Route::get('/arabic-language-teaching-course', 'Frontend\FrontendCoursesController@arabicLanguageCourses');
Route::get('/hifjul-quran', 'Frontend\FrontendCoursesController@hifjulQuran');
Route::get('/kaidah', 'Frontend\FrontendCoursesController@kaidah');
Route::get('/amparra', 'Frontend\FrontendCoursesController@amparra');
Route::get('/najerah', 'Frontend\FrontendCoursesController@najerah');
Route::get('/pre-hifz', 'Frontend\FrontendCoursesController@preHifz');

Route::get('/gallery', 'Frontend\FrontendMediaController@gallery');
Route::middleware(['auth','urlmiddleware'])->group(function () {    
    Route::get('/video-lecture', 'Frontend\FrontendMediaController@video');
    Route::get('/audio-lecture', 'Frontend\FrontendMediaController@audio');
    Route::get('/lecture-sheet', 'Frontend\FrontendMediaController@sheet');
});

Route::post('/contact-us/add','ContactController@store')->name('admin.contact.store');
Route::get('/about-us', 'Frontend\FrontendPostController@aboutUs');
Route::get('/contact-us', 'Frontend\FrontendPostController@contactUs');
Route::get('/blog', 'Frontend\FrontendPostController@blog');
Route::get('/{url}',[
    'as' => 'blog.post',
    'uses' => 'Frontend\FrontendPostController@blogDetails'
]);




/* Back end Admin panel Route */

Auth::routes();

Route::middleware(['auth', 'adminmiddleware'])->group(function () {

    Route::get('/admin/dashboard',[
        'as' => 'admin.dashboard',
        'uses' => 'DashboardController@index',
    ]);
    /*------------- User --------------*/
    Route::get('/admin/user',[
        'as' => 'admin.user',
        'uses' => 'UserController@index'
    ]);
    Route::get('admin/user/{id}', [
        'as'   => 'admin.edit',
        'uses' => 'UserController@edit'
    ]);
    Route::post('admin/user/{id}', [
        'as'   => 'admin.update',
        'uses' => 'UserController@update'
    ]);
    Route::any('/admin/user-delete/{id}',[
        'as' => 'admin.user.delete',
        'uses' => 'UserController@destroy'
    ]);
    /*------------- Page Section --------------*/
    Route::get('/admin/section',[
        'as' => 'admin.section',
        'uses' => 'SectionController@index'
    ]);
    Route::get('/admin/section/add',[
        'as' => 'admin.section.create',
        'uses' => 'SectionController@create'
    ]);
    Route::post('/admin/section/add',[
        'as' => 'admin.section.store',
        'uses' => 'SectionController@store'
    ]);
    Route::get('/admin/section/edit/{id}',[
        'as' => 'admin.section.edit',
        'uses' => 'SectionController@edit'
    ]);
    Route::post('/admin/section-update/{id}',[
        'as' => 'admin.section.update',
        'uses' => 'SectionController@update'
    ]);
    Route::any('/admin/section-delete/{id}',[
        'as' => 'admin.section.delete',
        'uses' => 'SectionController@destroy'
    ]);

    /*-------------Page Content --------------*/
    Route::get('/admin/content',[
        'as' => 'admin.content',
        'uses' => 'ContentController@index'
    ]);
    Route::get('/admin/content/add',[
        'as' => 'admin.content.create',
        'uses' => 'ContentController@create'
    ]);
    Route::post('/admin/content/add',[
        'as' => 'admin.content.store',
        'uses' => 'ContentController@store'
    ]);
    Route::get('/admin/content/edit/{id}',[
        'as' => 'admin.content.edit',
        'uses' => 'ContentController@edit'
    ]);
    Route::post('/admin/content-update/{id}',[
        'as' => 'admin.content.update',
        'uses' => 'ContentController@update'
    ]);
    Route::get('/admin/content-show/{id}',[
        'as' => 'admin.content.show',
        'uses' => 'ContentController@show'
    ]);
    Route::any('/admin/content-delete/{id}',[
        'as' => 'admin.content.delete',
        'uses' => 'ContentController@destroy'
    ]);

    /*------------- Image --------------*/
    Route::get('/admin/image',[
        'as' => 'admin.image',
        'uses' => 'PhotoController@index'
    ]);
    Route::get('/admin/image/add',[
        'as' => 'admin.image.create',
        'uses' => 'PhotoController@create'
    ]);
    Route::post('/admin/image/add',[
        'as' => 'admin.image.store',
        'uses' => 'PhotoController@store'
    ]);
    Route::get('/admin/image-change-status',[
        'as' => 'admin.image.change.status',
        'uses' => 'PhotoController@changeStatus'
    ]);
    Route::any('/admin/image-delete/{id}',[
        'as' => 'admin.image.delete',
        'uses' => 'PhotoController@destroy'
    ]);

    /*------------- Media --------------*/
    Route::get('/admin/media',[
        'as' => 'admin.media',
        'uses' => 'MediaController@index'
    ]);
    Route::get('/admin/media/add',[
        'as' => 'admin.media.create',
        'uses' => 'MediaController@create'
    ]);
    Route::post('/admin/media/add',[
        'as' => 'admin.media.store',
        'uses' => 'MediaController@store'
    ]);
    Route::get('/admin/media/edit/{id}',[
        'as' => 'admin.media.edit',
        'uses' => 'MediaController@edit'
    ]);
    Route::post('/admin/media-update/{id}',[
        'as' => 'admin.media.update',
        'uses' => 'MediaController@update'
    ]);
    Route::any('/admin/media-delete/{id}',[
        'as' => 'admin.media.delete',
        'uses' => 'MediaController@destroy'
    ]);
                /* PDF */
    Route::get('/admin/pdf',[
        'as' => 'admin.pdf',
        'uses' => 'PdfController@index'
    ]);
    Route::get('/admin/pdf/add',[
        'as' => 'admin.pdf.create',
        'uses' => 'PdfController@create'
    ]);
    Route::post('/admin/pdf/add',[
        'as' => 'admin.pdf.store',
        'uses' => 'PdfController@store'
    ]);
    Route::get('/admin/pdf/edit/{id}',[
        'as' => 'admin.pdf.edit',
        'uses' => 'PdfController@edit'
    ]);
    Route::post('/admin/pdf-update/{id}',[
        'as' => 'admin.pdf.update',
        'uses' => 'PdfController@update'
    ]);
    Route::any('/admin/pdf-delete/{id}',[
        'as' => 'admin.pdf.delete',
        'uses' => 'PdfController@destroy'
    ]);

    /*------------- Courses --------------*/
    Route::get('/admin/course',[
        'as' => 'admin.course',
        'uses' => 'CoursesController@index'
    ]);
    Route::get('/admin/course/add',[
        'as' => 'admin.course.create',
        'uses' => 'CoursesController@create'
    ]);
    Route::post('/admin/course/add',[
        'as' => 'admin.course.store',
        'uses' => 'CoursesController@store'
    ]);
    Route::get('/admin/course/edit/{id}',[
        'as' => 'admin.course.edit',
        'uses' => 'CoursesController@edit'
    ]);
    Route::post('/admin/course-update/{id}',[
        'as' => 'admin.course.update',
        'uses' => 'CoursesController@update'
    ]);
    Route::any('/admin/course-delete/{id}',[
        'as' => 'admin.course.delete',
        'uses' => 'CoursesController@destroy'
    ]);
    
    /*------------- Course Details--------------*/
    Route::get('/admin/course-details',[
        'as' => 'admin.course.details',
        'uses' => 'CourseDetailsController@index'
    ]);
    Route::get('/admin/course-details/add',[
        'as' => 'admin.course.details.create',
        'uses' => 'CourseDetailsController@create'
    ]);
    Route::post('/admin/course-details/add',[
        'as' => 'admin.course.details.store',
        'uses' => 'CourseDetailsController@store'
    ]);
    Route::get('/admin/course-details/edit/{id}',[
        'as' => 'admin.course.details.edit',
        'uses' => 'CourseDetailsController@edit'
    ]);
    Route::post('/admin/course-details-update/{id}',[
        'as' => 'admin.course.details.update',
        'uses' => 'CourseDetailsController@update'
    ]);
    Route::any('/admin/course-details-delete/{id}',[
        'as' => 'admin.course.details.delete',
        'uses' => 'CourseDetailsController@destroy'
    ]);
    

    /*------------- Contact Us --------------*/
    Route::get('/admin/contact',[
        'as' => 'admin.contact',
        'uses' => 'ContactController@index'
    ]);
    Route::any('/admin/contact/{id}',[
        'as' => 'admin.contact.delete',
        'uses' => 'ContactController@destroy'
    ]);

    /*------------- Backup Database--------------*/
    Route::get('/admin/backup',[
        'as' => 'admin.backup',
        'uses' => 'BackupController@index'
    ]);
    Route::post('/admin/backup',[
        'as' => 'admin.data.backup',
        'uses' => 'BackupController@backup'
    ]);   
       
    
    /*-------------Blog --------------*/
    Route::get('/admin/blog',[
        'as' => 'admin.blog',
        'uses' => 'BlogController@index'
    ]);
    Route::get('/admin/blog/add',[
        'as' => 'admin.blog.create',
        'uses' => 'BlogController@create'
    ]);
    Route::post('/admin/blog/add',[
        'as' => 'admin.blog.store',
        'uses' => 'BlogController@store'
    ]);
    Route::get('/admin/blog/edit/{id}',[
        'as' => 'admin.blog.edit',
        'uses' => 'BlogController@edit'
    ]);
    Route::post('/admin/blog-update/{id}',[
        'as' => 'admin.blog.update',
        'uses' => 'BlogController@update'
    ]);
    Route::get('/admin/blog-show/{id}',[
        'as' => 'admin.blog.show',
        'uses' => 'BlogController@show'
    ]);
    Route::any('/admin/blog-delete/{id}',[
        'as' => 'admin.blog.delete',
        'uses' => 'BlogController@destroy'
    ]);
    
});
