<header id="inner-navigation">
    <!-- navbar start -->
    <nav class="navbar navbar-default navbar-fixed-top navbar-sticky-function navbar-arrow">
        <div class="container">
            <div class="logo pull-left">
                <h2><a href="{{ url('/') }}"><img  class="logo-img" src="images/logo-white.png" alt=""><img class="logo-img logo-black" src="images/logo-black.png" alt=""></a></h2>
            </div>
            <div id="navbar" class="navbar-nav-wrapper pull-right">
                <ul class="nav navbar-nav navbar-right" id="responsive-menu">
                    <li class=""><a href="{{ url('/') }}">{{ __('Home') }}</a> </li>
                    <li><a href="{{ url('/about-us') }}">{{ __('About Us') }}</a></li>
                    <li>
                        <a href="#">Courses <i class="fa fa-angle-down"></i></a>
                        <ul>
                            <li><a href="{{ url('/quran-teaching-course')}}">{{ __('Quran Teaching Course') }} </a></li>
                            <li><a href="{{ url('/arabic-language-teaching-course')}}">{{ __('Arabic Language Teaching Course') }}</a></li>  
                            <li><a href="{{ url('/hifjul-quran')}}">{{ __('Hifjul Quran') }} </a></li>
                            <li><a href="{{ url('/pre-hifz')}}">{{ __('Pre-Hifz') }} </a></li>
                            <li><a href="{{ url('/kaidah')}}">{{ __('Kaidah') }}</a></li>
                            <li><a href="{{ url('/amparra')}}">{{ __('Amparra') }}</a></li> 
                            <li><a href="{{ url('/najerah')}}">{{ __('Najerah') }} </a></li>
                           
                        </ul>
                    </li>
                    <li>
                        <a href="#">Gallery <i class="fa fa-angle-down"></i></a>
                        <ul>
                            <li><a href="{{ url('/gallery') }}">{{ __('Image') }}</a></li>                           
                            <li><a href="{{ url('/video-lecture')}}">{{ __('Video Lecture') }}</a></li> 
                            <li><a href="{{ url('/lecture-sheet')}}">{{ __('Lecture Sheet') }}</a></li>                                
                        </ul>
                    </li>
                    <li><a href="{{ url('/blog') }}">{{ __('Blog') }}</a></li>
                    <li>
                        <a href="#">Apply<i class="fa fa-angle-down"></i></a>
                        <ul>                           
                            <li><a href="{{ route('register.form') }}">{{ __('Register / Log In') }}</a></li>
                            @Auth
                                <li>
                                   <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>   
                            @endauth                    
                        </ul>
                    </li>                    
                    <li><a href="{{ url('/contact-us') }}">{{ __('Contact') }}</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
        <div id="slicknav-mobile"></div>
    </nav>
    <!-- navbar end -->
</header>
