<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Institute of Quranic Studies">
    <meta property="og:image" content="{{asset('images/meta.jpg')}}"/>
    <meta property="og:image:width" content="180" />
    <meta property="og:image:height" content="110"/>
    <title> @yield('title') | Institute of Quranic Studies </title>    
    <link rel="icon" type="image/png" href="{{asset('images/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="36x36"  href="{{asset('images/android-icon-36x36.png')}}">
    
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/modal-video-min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/plugin.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet" type="text/css">  
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/flaticon1.css')}}" rel="stylesheet" type="text/css">
    @yield('css')
</head>
 <!--Other pages template-->
<body class="page">
        <!--*Header*-->
        @include('frontend.header')
        <!--* End Header*-->

        <!-- Banner -->
        <div id="blog_banner">
            <div class="page-title">
                <div class="container">
                    <h2> @yield('title') </h2>
                </div>
            </div>
            <div class="black-overlay"></div>
        </div>
        <!-- End banner -->

        @yield('pages')
    <!--*Footer*-->
    @include('frontend.footer')
    <!--* End Footer*-->
    <!-- back to top -->
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="" data-placement="left">
        <span class="fa fa-arrow-up"></span>
    </a>

    <!--*Scripts*-->

    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>    
    <script src="{{asset('js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="{{asset('js/jquery.nav.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/custom-magnificpopup.js')}}"></script>
    <script src="{{asset('js/slick.js')}}"></script>
    <script src="{{asset('js/slicknav.js')}}"></script>
    <script src="{{asset('js/custom-nav.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.appear.js')}}"></script>
    <script src="{{asset('js/isotope.min.js')}}"></script>
    <script src="{{asset('js/jquery.countTo.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>    
    
    <!-- This site is converting visitors into subscribers and customers with Rocketbots - https://rocketbots.io -->
    <script src="https://app.rocketbots.io/facebook/chat/plugin/19277/139498593154415" async></script>
    <!-- https://rocketbots.io/ -->

    @yield('scripts')
</body>
</html>