@extends('frontend.app')

@section('title')
    Qualified Teachers
@endsection


@section('pages')

   <!--*Features-one*-->
   <section class="features-one">
        <div class="container">
            <div class="inner-heading">
                <h3>Highly Qualified Teachers</h3>
                <h2>Various courses to choose from</h2>
            </div>


                <div class="col-md-4 col-sm-6 col-xs-12 item">
                    <div class="featured-item">
                        <div class="feat-img">
                            <img src="images/education/iqs-1.jpg" alt="">
                            <div class="th-name">
                                <h4>Economy from start for beginner</h4>
                            </div>
                            <div class="overlayPort">
                                <ul class="info text-center list-inline">
                                    <li>
                                        <a href="courses-detail.html">View Detail</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="featured-item">
                            <div class="feat-img">
                                <img src="images/education/iqs-2.jpg" alt="">
                                <div class="th-name">
                                    <h4>Economy from start for beginner</h4>
                                </div>
                                <div class="overlayPort">
                                    <ul class="info text-center list-inline">
                                        <li>
                                            <a href="courses-detail.html">View Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                         
                        </div>
                </div>

                
                <div class="col-md-4 col-sm-6 col-xs-12 item">
                    <div class="featured-item">
                        <div class="feat-img">
                            <img src="images/education/iqs-3.jpg" alt="">
                            <div class="th-name">
                                <h4>Economy from start for beginner</h4>
                            </div>
                            <div class="overlayPort">
                                <ul class="info text-center list-inline">
                                    <li>
                                        <a href="courses-detail.html">View Detail</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                     
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 item">
                    <div class="featured-item">
                        <div class="feat-img">
                            <img src="images/education/iqs-4.jpg" alt="">
                            <div class="th-name">
                                <h4>Economy from start for beginner</h4>
                            </div>
                            <div class="overlayPort">
                                <ul class="info text-center list-inline">
                                    <li>
                                        <a href="courses-detail.html">View Detail</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                     
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="featured-item">
                            <div class="feat-img">
                                <img src="images/education/iqs-1.jpg" alt="">
                                <div class="th-name">
                                    <h4>Economy from start for beginner</h4>
                                </div>
                                <div class="overlayPort">
                                    <ul class="info text-center list-inline">
                                        <li>
                                            <a href="courses-detail.html">View Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                            <div class="featured-item">
                                <div class="feat-img">
                                    <img src="images/education/iqs-2.jpg" alt="">
                                    <div class="th-name">
                                        <h4>Economy from start for beginner</h4>
                                    </div>
                                    <div class="overlayPort">
                                        <ul class="info text-center list-inline">
                                            <li>
                                                <a href="courses-detail.html">View Detail</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                             
                            </div>
                        </div>
                




        </div><!-- /.container -->
    </section>

    @endsection