@extends('frontend.app')

@section('title')
    Lecture Sheets
@endsection

@section('pages')

<section id="blog_main_sec" class="grid-view section-inner">
    <div class="container">
        <div class="inner-heading">
            <h3> Lecture Sheets</h3>
        </div>
        <div class="row">
            <!--*Blog Content Sec*-->
            <div class="col-md-12">
                <div class="row blog_post_sec">   

                    @foreach ($sheets as $sheet)  

                    <div class="col-md-4 col-sm-6 col-xs-12 grid-item">
                        <div class="blog-post_wrapper">
                        
                            <div class="blog-post-inner_wrapper">
                                <div class="blog-post-image">
                                    <div class="clearfix">
                                        <div class="embed-responsive embed-responsive-16by9 z-depth-1">
                                            <iframe class="embed-responsive-item"  src="/pdf/{{ $sheet->pdf }}"></iframe>
                                          </div>
                                    </div>
                                </div>
                                <div class="post-detail_container">
                                    <div class="post-content">
                                        <h3 class="post-title entry-title">
                                            <a href="/pdf/{{ $sheet->pdf }}" target="_blank"> {!! $sheet->title !!} </a>
                                        </h3>                                        
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    @endforeach                  

            </div>
            <!--* End Blog Content Sec*-->           
        </div>
    </div>
</section>
@endsection