

    <!--*Features-one*-->
    <section class="features-one">
        <div class="container">
            <div class="inner-heading">
                <h3>Related Courses</h3>
                <h2>There are variations available</h2>
            </div>
            <div class="row  slider slider-ft-course">
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="featured-item">
                            <div class="feat-img">
                                <img src="images/education/iqs-1.jpg" alt="">
                                <div class="th-name">
                                    <h4>Kaidah</h4>
                                </div>
                                <div class="overlayPort">
                                    <ul class="info text-center list-inline">
                                        <li>
                                            <a href="{{ url('/course-kaidah')}}">View Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                            <div class="featured-item">
                                <div class="feat-img">
                                    <img src="images/education/iqs-2.jpg" alt="">
                                    <div class="th-name">
                                        <h4>Amparra</h4>
                                    </div>
                                    <div class="overlayPort">
                                        <ul class="info text-center list-inline">
                                            <li>
                                                <a href="{{ url('/course-amparra')}}">View Detail</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                             
                            </div>
                        </div>
    
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="featured-item">
                            <div class="feat-img">
                                <img src="images/education/iqs-3.jpg" alt="">
                                <div class="th-name">
                                    <h4>Najerah</h4>
                                </div>
                                <div class="overlayPort">
                                    <ul class="info text-center list-inline">
                                        <li>
                                            <a href="{{ url('/course-najerah')}}">View Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="featured-item">
                            <div class="feat-img">
                                <img src="images/education/iqs-4.jpg" alt="">
                                <div class="th-name">
                                    <h4>Arabian Hifjul Quran</h4>
                                </div>
                                <div class="overlayPort">
                                    <ul class="info text-center list-inline">
                                        <li>
                                            <a href="{{ url('/course-hifjul-quran')}}">View Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>                     
                        </div>
                    </div>   
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                            <div class="featured-item">
                                <div class="feat-img">
                                    <img src="images/education/iqs-4.jpg" alt="">
                                    <div class="th-name">
                                        <h4>As Salah & Daily Prayer</h4>
                                    </div>
                                    <div class="overlayPort">
                                        <ul class="info text-center list-inline">
                                            <li>
                                                <a href="{{ url('/course-daily-prayer')}}">View Detail</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>                     
                            </div>
                        </div>            
                </div>
            

        </div><!-- /.container -->
    </section>
    <!--*EndFeatures-one*-->


