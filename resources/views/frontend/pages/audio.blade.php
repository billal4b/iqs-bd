@extends('frontend.app')

@section('title')
    Audio lecture
@endsection

@section('pages')

<section id="blog_main_sec" class="grid-view section-inner">
    <div class="container">
        <!--flash Message-->
        @include('flash-message')
        <!-- section title -->
        <div class="inner-heading">
            <h3>Audio lecture </h3>
        </div>
        <div class="row">
            <!--*Blog Content Sec*-->
            <div class="col-md-12">
                <div class="row blog_post_sec"> 

                    @foreach ($audios as $audio)              
                   
                        <div class="col-md-4 col-sm-6 col-xs-12 grid-item">
                            <div class="blog-post_wrapper">
                                <div class="blog-post-inner_wrapper">
                                    <div class="blog-post-image">
                                        <div class="clearfix">
                                            <div class="embed-responsive embed-responsive-16by9 z-depth-1">
                                                <iframe class="embed-responsive-item" src="{!! $audio->audio !!}" style="height: 101%"
                                                allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-detail_container">
                                        <div class="post-content">
                                            <h3 class="post-title entry-title">
                                                {!! $audio->title !!}
                                            </h3>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
            <!--* End Blog Content Sec*-->           
        </div>
    </div>
</section>


@endsection