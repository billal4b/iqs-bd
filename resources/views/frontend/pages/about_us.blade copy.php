@extends('frontend.app')

@section('title')
    About Us
@endsection

@section('pages')
    
    <!--*About*-->
    <section id="mt_about">
        <div class="container">
            <div class="about_services">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="about-items">
                            <div class="inner-heading">
                                <h3>Why You Should Join  Institute Of Quranic Studies?</h3>
                            </div>
                            <p>At The Institute Of Quranic Studies (IQS), each student gets special attention, depending on their learning need, by assigning a professional Quran tutor for each student.
                             
                        </div>
                    </div>
                 
                </div>                    
            </div>
        </div>
    </section>
        <!--*EndAbout*-->
    
    <!--*About*-->
 <section id="mt_about">
        <div class="container">
            <div class="about_services">
                <div class="row">                  
                    <div class="col-xs-12">
                        <div class="about-form">
                            <div class="col-sm-9">
                                <div class="about-sch-form">
                                    <div class="event-title">
                                        <h2>Apply for Admission</h2>
                                        <p>This is the golden opportunity of reciting Al-Quran having with pure accent & melodious voice and of establishing precise salah</p>
                                    </div> <!-- event title -->                                    
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="smt-items mar-top-40">
                                    <a class="mt_btn_yellow" href="{{ url('/registration') }}">Apply Here</a>
                                </div>
                            </div>
                        </div>                        
                    </div>

                    
                </div>
            </div>
        </div>
    </section>
    <!--*EndAbout*-->
<!--* Mission Area*-->
<section id="mission_main" class="mission-area section-padding"> 
    <div class="container">
        <div class="row">        
            <div class="col-xs-12">                          
                <div class="mission-content">
                    <div class="inner-heading">
                        <h3>Our Mission</h3>
                    </div>
                    <p>The Quran is the most important book in Islam. It is a sacred book of Islam and because of this, it is very important for all those who follow Islam to learn how to read, write and understand the Quran. </p><br>
                    <p>we are committed to helping people not only learn how to memorize Quran, learn to recite Quran and other learning of Quran. By allowing students to learn in the comfort of their own home, at their own pace and when they want to do it, we believe it is possible to understand all of the rules that surround learning the Quran.</p><br>
                    <p>We also realize that learning the Quran is only the start for some. By offering Arabic languages IQS and Arabic speaking lessons, it is possible for people to master this difficult but important language. Our IQS Quran learning for kids programs can help the younger generation. Offering both learning the Quran for beginners and more advanced courses allows people of all levels to complete the process.</p><br>
                    <p>While we want people to be able to learn to Quran, we also want to make sure that all of the students, whether it is kids learning Quran or adults, are able to learn how to do it properly. It is considered disrespectful to make mistakes about the Quran, and we make sure our IQS courses will prevent that from happening. We strive to give our students the knowledge and expertise that they need for the Quran.</P>
                </div>                          
            </div>
        </div>
    </div>
</section>
<!--* End Mission Area*-->
<!--* Vision Area*-->
<section id="mission_main" class="mission-area section-padding"> 
    <div class="container">           
        <div class="row">
            <div class="col-xs-12">                          
                <div class="mission-content">
                    <div class="inner-heading">
                        <h3>Our Vision</h3>
                    </div>
                        <p>To ensure students as a real human being in the society through reciting Al-Quran & teaching morals along with modern study is the goal of IQS.</p>
                </div>                       
            </div>
        </div> 
    </div>
</section>
<!--* End Vision Area*-->

<!--* We offer*-->
<section id="mission_main" class="mission-area section-padding"> 
        <div class="container">           
            <div class="row">
                <div class="col-xs-12">                          
                    <div class="mission-content">
                        <div class="inner-heading">
                            <h3>Easily Learn Quranic Arabic by Taking Our IQS Courses</h3>
                        </div>
                            <p>Do you have a keen interest or desire to learn how to speak or write the Arabic language Quran? Well, now that is a possibility. At IQS, we now offer several courses that are tailored to teaching both the adult and younger audiences. </p>
                    </div>                       
                </div>
            </div> 
        </div>
    </section>
    <!--* We offer*-->
</section>

<!--* We offer*-->
<section id="mission_main" class="mission-area section-padding"> 
        <div class="container">           
            <div class="row">
                <div class="col-xs-12">                          
                    <div class="mission-content">
                        <div class="inner-heading">
                            <h3>What Other Courses Do We Offer?</h3>
                        </div>
                            <p>As well as offering courses that teach you how to speak Arabic, we also offer other additional courses to help you with the following:</p><br>
                            <ul>
                                <li>1. Quran Recitation – Quran memorization and practice, learn to recite the Quran.</li>
                                <li>2. Arabic Speaking Course – Learn how to speak basic Arabic.</li>
                                <li>3. Quran for Kids – We have a different method of teaching kids the Quran</li>
                            </ul>
                            <p> What’s more is that all of our teachers are experienced native Arabic speakers and are some of the best Quran tutors available. We provide female teachers for female students and male teachers for male students.<p/>
                    </div>                       
                </div>
            </div> 
        </div>
    </section>
    <!--* We offer*-->
</section>

@endsection