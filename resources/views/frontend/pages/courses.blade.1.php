@extends('frontend.app')

@section('title')
    Courses
@endsection

@section('pages')

   <!--*Features-one*-->
   <section class="features-one">
        
        
    <div class="container">
        <div class="inner-heading">
            <h3>Featured courses</h3>
            <h2>Various courses to choose from</h2>
        </div>

     

    </div><!-- /.container -->

    <br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-down">
                    <div class="event-title">
                        <h2>Kaidah</h2>
                        <h3 class="white">Learning Al Quran (Part-1):</h3>
                        <p>Introduction & description of Arabic letters, Harakat, Noon-sukkin, Tanvir, Jajam, Tasdid, including standard pronunciation (Makhraj)</p>
                        <p>Introduction of (Iman) Believe along with kalimah.</p>
                    </div> <!-- event title -->                                   
                    
                </div>                        
            </div>
        </div>
    </div>
<br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-up">
                    <div class="event-title">
                        <h2>Amparra</h2>
                        <h3 class="white">30th Para of Al-Quran (Part- 2):</h3>
                        <p>Learning, spelling, reciting of 11 surah from surah Al Fathea to surah feel.</p>
                        <p>Learning & spelling surah from Humajah to Naba.</p>
                    </div> <!-- event title -->                                    
                                 
                </div>                        
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-down">
                    <div class="event-title">
                        <h2>Najerah</h2>
                        <h3 class="white">Efficient reciting of Al-Quran (final Part):</h3>
                        <p> Reciting Al-Quran from surah Al-Bakarah to whole Quran.</p>
                    </div> <!-- event title -->                                   
                    
                </div>                        
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-up">
                    <div class="event-title">
                        <h2>Arabian Hifjul Quran</h2>
                        <p> step-1: 30th para, step-2: 29th para, step-3: 28th para, Last step: from 1st para to whole Quran.</p>
                    </div> <!-- event title -->                                   
                    
                </div>                        
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-down">
                    <div class="event-title">
                        <h2>As Salah & Daily Prayer</h2>
                        <p> Memorizing of prayers & tasbih for sarah.</p>
                        <p> Learning necessary steps for salah.</p>
                        <p> Teaching necessary Hadith.</p>
                        <p> Teaching basic & norms of Al-Quran.</p>
                    </div> <!-- event title -->                                   
                    
                </div>                        
            </div>
        </div>
    </div>
<br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="course-color-up">
                    <div class="event-title">
                        <p style="text-align: center; font-size: 18px">There is also a learning opportunity for guardians & Adults.</p>
                        
                    </div> <!-- event title -->                                   
                    
                </div>                        
            </div>
        </div>
    </div>

</section>

    @endsection