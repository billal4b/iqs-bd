@extends('backend.layouts.template')

@section('main-content')
<div class="row">
    @include('flash-message')
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="dashboard-list-box">
            <h4 class="gray">PDF File List <a href="{{ route('admin.pdf.create') }}" ><span class="button gray">Add</span></a>
            </h4>
            <div class="table-box">
            <table class="basic-table booking-table">
                <thead>
                    <tr>
                        <th>Title</th>      
                        <th>Course</th> 
                        <th>Order</th> 
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>                   
                      @foreach ($medias as $media)
                      <tr>                
                          <td>{!! $media->title !!}</td> 
                          <td>{!! $media->course_title !!}</td>   
                          <td>{!! $media->order !!}</td>  
                          <td><span class="{{ $media->is_active == 1 ? 'paid' : 'cancel' }} t-box">{!! $media->is_active == 1 ? 'Active' : 'Inactive' !!}</span></td>
                          <td>                         
                            <a href="{{ route('admin.pdf.edit', $media->id) }}" class="button"> Edit</a>
                            <a href="{{ route('admin.pdf.delete', $media->id) }}" class="button" onclick="return confirm('Are you sure to Delete?')"> Delete</a>
                          </td>       
                      </tr>    
                      @endforeach       
                </tbody>
            </table>
            
            </div>
        </div>
        {!! $medias->links() !!}       
    </div>
</div>
@endsection