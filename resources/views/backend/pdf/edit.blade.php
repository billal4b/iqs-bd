@extends('backend.layouts.template')
@section('css')

@endsection
@section('main-content')    
<div class="dashboard-form">
        <div class="row">
            <form  action="{{ route('admin.pdf.update', $edit->id) }}" method="post" enctype="multipart/form-data">
                @csrf                
            <!-- Profile -->
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="dashboard-list-box">
                    <h4 class="gray">Update Media <a href="{{ route('admin.pdf') }}" ><span class="button gray">List</span></a></h4>
                    <div class="dashboard-list-box-static">
                        
                        <!-- Details -->
                        <div class="my-profile">

                            <label for="title">{{ __('Title') }}</label>
                            <input id="title" name="title" type="text" value="{{ $edit->title }}" class="form-control @error('title') is-invalid @enderror" required autocomplete="title" autofocus>
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror   

                            <label for="course_title">{{ __('Course Name') }}</label>
                            <select  id="course_title" name="course_title" class="form-control">
                                @foreach ($courses as $course)
                                    <option value="{{ $course->course_name }}">{{ $course->course_name }}</option>
                                @endforeach
                            </select>
                                @error('course_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 

                           

                            <label for="pdf">{{ __('Lecture Sheet ( pdf format)') }}</label>
                                <input id="pdf" name="pdf" type="file" class="form-control @error('pdf') is-invalid @enderror" autocomplete="pdf" accept="application/pdf" autofocus>
                                
                                @error('pdf')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                                <iframe src="/pdf/{{ $edit->pdf }}" height="150" width="150"></iframe>

                            <label for="order">{{ __('Order') }}</label>
                            <input id="order" name="order" type="number" value="{{ $edit->order }}" class="form-control @error('order') is-invalid @enderror"  autocomplete="order" autofocus>
                                @error('order')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 

                            <label for="is_active">{{ __('Status') }}</label>
                            <select id="type" name="is_active" class="form-control">    
                                <option value="1" {{ $edit->is_active == 1 ? 'selected' : '' }}>Active</option>
                                <option value="0" {{ $edit->is_active == 0 ? 'selected' : '' }}>Inactive</option>
                            </select> 
                            
                        </div>
                        <button type="submit" class="button">{{ __('Save') }}</button>

                        </div>                       
                    </div>
                </div>
            </div>            
            </form>
        </div>
    </div>  
    
@endsection
@section('scripts')

<script type="text/javascript">

document.getElementById('course_title').value = '{{ $edit->course_title }}';


</script>
@endsection