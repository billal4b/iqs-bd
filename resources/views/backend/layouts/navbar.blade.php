<div class="dashboard-nav">
    <div class="dashboard-nav-inner">
        <ul>
            <li class="{{ Request::segment(2) == 'dashboard' ? 'active' : '' }}"><a href="{{ route('admin.dashboard') }}"><i class="sl sl-icon-settings"></i>{{ __('Dashboard') }} </a></li>
            <li class="{{ Request::segment(2) == 'user' ? 'active' : '' }}"><a href="{{ route('admin.user') }}"><i class="sl sl-icon-user"></i>{{ __('Users') }}</a></li>    
            <li class="{{ Request::segment(2) == 'blog' ? 'active' : '' }}"><a href="{{ route('admin.blog') }}"><i class="sl sl-icon-folder"></i> Blog</a></li>    
                 
            <li class="{{ Request::segment(2) == 'image' ? 'active' : '' | ( Request::segment(2) == 'media' ? 'active' : '') | ( Request::segment(2) == 'pdf' ? 'active' : '') }}">
                <a><i class="sl sl-icon-camera"></i>{{ __('Media') }}<span class="slicknav_arrow"><i class="sl sl-icon-plus"></i></span></a>
                <ul>
                    <li><a href="{{ route('admin.image') }}">{{ __('Images') }}</a></li>
                    <li><a href="{{ route('admin.media') }}">{{ __('Video') }}</a></li>
                    <li><a href="{{ route('admin.pdf') }}">{{ __('PDF') }}</a></li>
                </ul>
            </li>
            <li class="{{ Request::segment(2) == 'course' ? 'active' : '' | ( Request::segment(2) == 'course-details' ? 'active' : '') }}">
                <a><i class="sl sl-icon-layers"></i>{{ __('Courses') }}<span class="slicknav_arrow"><i class="sl sl-icon-plus"></i></span></a>
                <ul>
                    <li><a href="{{ route('admin.course') }}">{{ __('Course Name') }}</a></li>
                    <li><a href="{{ route('admin.course.details') }}">{{ __('Course Details') }}</a></li>
                </ul>
            </li>    
            <li class="{{ Request::segment(2) == 'section' ? 'active' : '' | ( Request::segment(2) == 'content' ? 'active' : '') }}">
                <a><i class="sl sl-icon-list"></i>{{ __('Section') }}<span class="slicknav_arrow"><i class="sl sl-icon-plus"></i></span></a>
                <ul>                    
                    <li><a href="{{ route('admin.section') }}">{{ __('Page Section') }}</a></li>
                    <li><a href="{{ route('admin.content') }}">{{ __('Page Content') }}</a></li>
                </ul>
            </li>      
                           
            <li class="{{ Request::segment(2) == 'contact' ? 'active' : '' }}"><a href="{{ route('admin.contact') }}"><i class="sl sl-icon-envelope-open"></i> Contact</a></li>  
            <li class="{{ Request::segment(2) == 'backup' ? 'active' : '' }}"><a href="{{ route('admin.backup') }}"><i class="sl sl-icon-arrow-down-circle"></i> Backups</a></li> 
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                    <i class="sl sl-icon-power"></i>{{ __('Logout') }}
                </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </div>
</div>

