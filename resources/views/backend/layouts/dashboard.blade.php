@extends('backend.layouts.template')

@section('main-content')  
  
 
    <div class="row">
        <!--flash Message-->
        @include('flash-message')
        <div class="row">

            <!-- Item -->
            <div class="col-lg-3 col-md-6 col-xs-6">
                <div class="dashboard-stat color-1">
                    <div class="dashboard-stat-content"><h4>6</h4> <span>Active Users</span></div>
                    <div class="dashboard-stat-icon"><i class="im im-icon-Map2"></i></div>
                </div>
            </div>

            <!-- Item -->
            <div class="col-lg-3 col-md-6 col-xs-6">
                <div class="dashboard-stat color-2">
                    <div class="dashboard-stat-content"><h4>726</h4> <span>Total User</span></div>
                    <div class="dashboard-stat-icon"><i class="im im-icon-Line-Chart"></i></div>
                </div>
            </div>


            <!-- Item -->
            <div class="col-lg-3 col-md-6 col-xs-6">
                <div class="dashboard-stat color-3">
                    <div class="dashboard-stat-content"><h4>95</h4> <span>Total Reviews</span></div>
                    <div class="dashboard-stat-icon"><i class="im im-icon-Add-UserStar"></i></div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-xs-6">
                <div class="dashboard-stat color-4">
                    <div class="dashboard-stat-content"><h4>126</h4> <span>Bookmarks</span></div>
                    <div class="dashboard-stat-icon"><i class="im im-icon-Heart"></i></div>
                </div>
            </div>
        </div>

    </div>


@endsection
    