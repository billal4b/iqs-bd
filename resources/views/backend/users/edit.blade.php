@extends('backend.layouts.template')

@section('main-content')
    

<div class="dashboard-form">
        <div class="row">
            <form  action="{{ route('admin.update', $edit->id) }}" method="post">
            @csrf 
               
            <!-- Profile -->
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="dashboard-list-box">
                    <h4 class="gray">Student Information</h4>
                    <div class="dashboard-list-box-static">
                        
                        <!-- Details -->
                        <div class="my-profile">

                            <label for="name">{{ __('Full Name') }}</label>
                            <input id="name" name="name" value="{{ $edit->name }}" type="text" class="form-control @error('name') is-invalid @enderror" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="mobile">{{ __('Mobile Number') }}</label>
                            <input id="mobile" name="mobile" value="{{ $edit->mobile }}" type="text" class="form-control @error('mobile') is-invalid @enderror" required autocomplete="mobile number" autofocus>
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                            
                           
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" name="email" value="{{ $edit->email }}" type="email" class="form-control @error('email') is-invalid @enderror" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="address">{{ __('Address') }}</label>
                            <input id="address" name="address" value="{{ $edit->address }}" type="text" class="form-control @error('address') is-invalid @enderror" required autocomplete="address" autofocus> 
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror"  required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm"  name="password_confirmation" type="password" class="form-control" required autocomplete="new-password">
                                

                        </div>                       
                    </div>
                </div>
            </div>

            <!-- Change Password -->
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Update <a href="{{ route('admin.user') }}" ><span class="button gray">List</span></a></h4>
                    <div class="dashboard-list-box-static">

                        <div class="my-profile">
                            <label for="course_title">{{ __('Course Name') }}</label>
                            <select name="course_title" id="course_title" class="form-control">
                                    <option value="">- Select -</option>    
                                @foreach($courses as $course )                                                                    
                                    <option value="{{ $course->course_name }}">{{ $course->course_name }} </option>
                                @endforeach
                            </select>   
                            @error('course_title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror 

                            <label for="gender">{{ __('Gender') }}</label>
                            <select name="gender" class="form-control">    
                                <option value="male" {{ $edit->gender == 'male' ? 'selected' : '' }}>Male</option>
                                <option value="female" {{ $edit->gender == 'female' ? 'selected' : '' }}>Female</option>
                            </select> 
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            <label for="blood_group">{{ __('Blood Group') }}</label>
                            <select name="blood_group" class="form-control">    
                                <option value="a+" {{ $edit->blood_group == 'a+' ? 'selected' : '' }}>A + </option>
                                <option value="b+" {{ $edit->blood_group == 'b+' ? 'selected' : '' }}>B + </option>
                                <option value="o+" {{ $edit->blood_group == 'o+' ? 'selected' : '' }}>O + </option>
                                <option value="ab+" {{ $edit->blood_group == 'ab+' ? 'selected' : '' }}>AB + </option>
                                <option value="a-" {{ $edit->blood_group == 'a-' ? 'selected' : '' }}>A - </option>
                                <option value="b-" {{ $edit->blood_group == 'b-' ? 'selected' : '' }}>B - </option>
                                <option value="o-" {{ $edit->blood_group == 'o-' ? 'selected' : '' }}>O - </option>
                                <option value="ab-" {{ $edit->blood_group == 'ab-' ? 'selected' : '' }}>AB - </option>
                            </select> 
                                @error('blood_group')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="dob">{{ __('Date of Birth') }}</label>
                            <input id="dob" name="dob" type="date" value="{{ $edit->dob }}" class="form-control @error('dob') is-invalid @enderror" autocomplete="address" autofocus> 
                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="nid_no">{{ __('NID') }}</label>
                            <input id="nid_no" name="nid_no" value="{{ $edit->nid_no }}" type="number" class="form-control @error('address') is-invalid @enderror" autocomplete="address" autofocus> 
                                @error('nid_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                               

                            <label for="status">{{ __('Status') }}</label>
                            <select name="status" class="form-control">    
                                <option value="active" {{ $edit->status == 'active' ? 'selected' : '' }}>Approved</option>
                                <option value="inactive" {{ $edit->status == 'inactive' ? 'selected' : '' }}>Pending</option>
                            </select> 
                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                                              

                        </div>
                        <button type="submit" class="button">{{ __('Update') }}</button>

                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>  
    
@endsection

@section('scripts')
<script type="text/javascript">
    document.getElementById('course_title').value = '{{ $edit->course_title }}';
</script>
@endsection