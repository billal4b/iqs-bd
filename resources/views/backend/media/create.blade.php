@extends('backend.layouts.template')
@section('css')
<style>
.audio-file, .video-file, .pdf-file {
    display:none;
}
</style>
@endsection
@section('main-content')    
<div class="dashboard-form">
        <div class="row">
            <form  action="{{ route('admin.media.store') }}" method="post" enctype="multipart/form-data">
                @csrf                
            <!-- Profile -->
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="dashboard-list-box">
                    <h4 class="gray">Add Media <a href="{{ route('admin.media') }}" ><span class="button gray">List</span></a></h4>
                    <div class="dashboard-list-box-static">
                        
                        <!-- Details -->
                        <div class="my-profile">

                            <label for="title">{{ __('Title') }}</label>
                            <input id="title" name="title" type="text" class="form-control @error('title') is-invalid @enderror" required autocomplete="title" autofocus>
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror   

                            <label for="course_title">{{ __('Course Name') }}</label>
                            <select  id="course_title" name="course_title" class="form-control">
                                @foreach ($courses as $course)
                                    <option value="{{ $course->course_name }}">{{ $course->course_name }}</option>
                                @endforeach
                            </select>
                                @error('course_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 



                            <label for="file_type">{{ __('Select Media Format') }}</label>
                            <select id="file_type" name="file_type" class="form-control">    
                                <option>-- Select --</option>
                                <option value="audio">Audio File</option>
                                <option value="video">Video File</option>
                            </select>                               
                             
                            <div id="audio-file" style="display: none">
                                <label for="audio">{{ __('Audio URL') }}</label>
                                <input id="audio" name="audio" type="text" class="form-control @error('audio') is-invalid @enderror" autocomplete="audio" autofocus>
                                @error('audio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror    
                            </div>  
                            <div id="video-file" style="display: none">
                                <label for="video">{{ __('YouTube Video Embed URL') }}</label>
                                <input id="video" name="video" type="text" class="form-control @error('video') is-invalid @enderror" autocomplete="video" autofocus>
                                
                                @error('video')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror    
                            </div> 
                            
                        </div>
                        <button type="submit" class="button">{{ __('Save') }}</button>

                        </div>                       
                    </div>
                </div>
            </div>
            
            </form>
        </div>


    </div>  
    
@endsection
@section('scripts')

<script type="text/javascript">

 $('#file_type').change(function(){
       
        if( $(this).val() == 'audio'){
                $('#audio-file').show();
        }else{
            $('#audio-file').hide();
        }
        
        if( $(this).val() == 'video'){
            $('#video-file').show();
        }else{
            $('#video-file').hide();
        }

    });

</script>
@endsection