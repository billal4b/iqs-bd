<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Institute of Quranic Studies">
    <title>Login | IQS-BD</title>
    <link href="{{asset('images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body style="background-color: #071c29fa;">
    <section class="section-inner" style="margin-top: 100px;">
        <div class="container">
            <div class="row" >
                <div class="col-md-3 col-sm-4 col-xs-12"> </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="background-color: #fff; padding:20px;">
                    <div class="account-inner">
                        <!-- section title -->
                        <div class="inner-heading">
                            <h3> Sign In </h3>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="mt_btn_yellow">{{ __('Login') }}</button>

                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>

                            </div>
                            @if (Route::has('password.request'))
                                <p><a class="lost_password" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a></p>
                            @endif
                            <h3><a href="{{ url('/') }}"> << - IQSBD</a></h3>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12"> </div>
            </div>
        </div>
    </section>
    <!-- End store -->
</body>
</html>
