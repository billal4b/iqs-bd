USE iqs_bd



TRUNCATE blogs;

INSERT INTO blogs VALUES ('1','Apply for Admission Header title','apply-for-admission-header-title','Read the Latest Research & Guidance for Identifying Vulnerabilities','<p>Read the Latest Research &amp; Guidance for Identifying Vulnerabilities</p>',NULL,NULL,'2020-01-07','1579259300.png','1579259300.png',NULL,'1','2020-01-17 11:08:20','2020-01-17 11:08:20');

TRUNCATE contents;

INSERT INTO contents VALUES ('1','Apply for Admission Header title','<p>Read the Latest Research &amp; Guidance for Identifying Vulnerabilities</p>','Header Text',NULL,'1','2020-01-17 11:08:48','2020-01-17 11:08:48',NULL),('2','fasfa','asfasf','Header Title',NULL,'1',NULL,NULL,NULL),('3','IQS','About Us','About Us',NULL,'1',NULL,NULL,NULL),('4','Admission','Admission','Admission',NULL,'1',NULL,NULL,NULL),('5','Footer One','Footer One','Footer One',NULL,'1',NULL,NULL,NULL),('6','Footer Two','Footer Two','Footer Two',NULL,'1',NULL,NULL,NULL);

TRUNCATE course_details;

INSERT INTO course_details VALUES ('1','Amphara','<p>https://www.youtube.com/embed/1arCsrFgGKI</p>','<p>https://www.youtube.com/embed/1arCsrFgGKI</p>',NULL,'1','2020-01-17 11:12:33','2020-01-17 11:12:33');

TRUNCATE courses;

INSERT INTO courses VALUES ('1','Amphara','amphara',NULL,NULL,'1','2020-01-16 11:40:04','2020-01-16 11:40:04'),('2','Najerah 1','najerah-1',NULL,NULL,'1','2020-01-17 11:12:16','2020-01-17 11:12:16'),('3','Kaidah','kaidah','1579689456.jpg','1579689456.jpg','1','2020-01-22 10:37:37','2020-01-22 10:37:37'),('4','Najerah','najerah','1579689470.jpg','1579689470.jpg','1','2020-01-22 10:37:50','2020-01-22 10:37:50');

TRUNCATE media;

INSERT INTO media VALUES ('1','Apply for Admission Header title','Amphara','video',NULL,'https://www.youtube.com/embed/hrAUGwXsjUg?controls=0',NULL,NULL,'1','2020-01-16 11:40:22','2020-01-16 11:40:22'),('2','World-Class Penetration Testing Execution and Delivery','Amphara','audio','https://www.youtube.com/embed/1arCsrFgGKI',NULL,NULL,NULL,'1','2020-01-17 11:11:52','2020-01-17 11:11:52');

TRUNCATE migrations;

INSERT INTO migrations VALUES ('1','2014_10_12_000000_create_users_table','1'),('2','2014_10_12_100000_create_password_resets_table','1'),('3','2019_08_19_000000_create_failed_jobs_table','1'),('4','2019_12_06_144521_create_sections_table','1'),('5','2019_12_06_144609_create_contents_table','1'),('6','2019_12_06_145544_create_media_table','1'),('7','2019_12_28_203938_create_photos_table','1'),('8','2019_12_30_192528_create_courses_table','1'),('9','2019_12_30_194956_create_blogs_table','1'),('10','2020_01_01_132045_create_course_details_table','1'),('11','2020_01_04_131546_create_contacts_table','1');

TRUNCATE photos;

INSERT INTO photos VALUES ('1','Apply','gallery','gallery_Apply.jpg','gallery_Apply.jpg',NULL,'1','2020-01-17 11:25:15','2020-01-17 11:25:46');

TRUNCATE sections;

INSERT INTO sections VALUES ('1','Header Text','header text','1','2020-01-17 11:03:46','2020-01-17 11:03:46'),('2','Header Text','header-text','1','2020-01-22 10:38:51','2020-01-22 10:38:51'),('3','About us','about-us','1','2020-01-22 10:38:56','2020-01-22 10:38:56'),('4','Admission','admission','1','2020-01-22 10:38:59','2020-01-22 10:38:59');

TRUNCATE users;

INSERT INTO users VALUES ('1','Billal Hossain','1676747386','billal@yahoo.com',NULL,'$2b$10$5t0PjCqYfzFqFdFGIYPwI.VtsKRfMAwlhRQ0kJpqxeC6VGOWAS5i2','Gulshan -2',NULL,NULL,NULL,NULL,NULL,NULL,'1','active','1','0',NULL,'2020-01-16 11:10:17','2020-01-16 11:10:17');