<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Media;
Use App\Course;

class PdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminmiddleware');        
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medias = Media::orderBy('id', 'desc')->where('file_type','pdf')->paginate(20);
        $courses = Course::where('is_active',1)->pluck('course_name','id')->all();
        return view('backend.pdf.index', compact('medias','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::where('is_active', 1)->get();     
        return view('backend.pdf.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $request->validate([
            'title' => 'required', 
            'course_title' => 'required', 
        ]); 

        $media = new Media();            
        $media->title = $request->title;
        $media->course_title  = $request->course_title;
        $media->file_type  = 'pdf';
        $media->video  = $request->video;
        $media->audio  = $request->audio;
     
        if( $file = $request->file('pdf')) {         
            $name = implode('_',explode(' ',$request->title));
            $name = $request->file_type.'_'.$name.'.'.$file->getClientOriginalExtension();     
            $destinationPath = public_path('/pdf');
            $file->move($destinationPath, $name);   

            $media->pdf = $name;
        } 
        $media->save();
        return redirect('/admin/pdf')->with('success', 'File has been saved!');
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Media::findOrFail($id);
        $courses = Course::where('is_active', 1)->get();
        return view('backend.pdf.edit', compact('edit','courses'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'course_title' => 'required',
            'is_active'    => 'required',
        ]);

        $media = Media::findOrFail($id);  
        $media->title = $request->title;
        $media->course_title  = $request->course_title;
        $media->file_type  = 'pdf';
        $media->video  = $request->video;
        $media->audio  = $request->audio;
        $media->order  = $request->order;
        $media->is_active  = $request->is_active;

        if( $file = $request->file('pdf')) {         
            $name = implode('_',explode(' ',$request->title));
            $name = $request->file_type.'_'.$name.'.'.$file->getClientOriginalExtension();     
            $destinationPath = public_path('/pdf');
            $file->move($destinationPath, $name);   

            $media->pdf = $name;
        } 
        
        $media->update();    
        return redirect('/admin/pdf')->with('success', 'File has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::findOrFail($id);
        $media->delete();

        return redirect('/admin/pdf')->with('success', 'File has been deleted');
    }
}
