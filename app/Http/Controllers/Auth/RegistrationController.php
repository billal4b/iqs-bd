<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerForm()
    {
        return view('auth.registration');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function registerCreate(Request $request)
    { 
        $request->validate([            
            'name'     => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile'   => ['required','min:11','max:11'],
            //'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $create = [
            'name' => $request->name, 
            'email' => $request->email,
            'mobile' => $request->mobile,
            //'password' => Hash::make($request->password),
            'address' => $request->address                       
        ];
        
        user::create($create);
        return back()->with('success','Successfully applied. You will be contacted!');
    }

}
