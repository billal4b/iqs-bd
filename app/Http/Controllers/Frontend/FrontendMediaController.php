<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Media;
Use App\Course;
Use App\Photo;

class FrontendMediaController extends Controller
{
   
    public function audio()
    {
        $courses = Course::where('is_active',1)->pluck('course_name','id')->all();
        $course_name = auth()->user()->course_title;   

        if( auth()->user()->is_admin == 1 ) {

            $audios = Media::select('title','course_title','audio','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'audio')
                    ->orderBy('order', 'desc')
                    ->get();
        }else{
            $audios = Media::select('title','course_title','audio','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'audio')
                    ->where('course_title', $course_name)
                    ->orderBy('order', 'desc')
                    ->get();
        }
        return view('frontend.pages.audio',compact('courses','audios'));
    }

    public function video()
    {
        $courses = Course::where('is_active',1)->pluck('course_name','id')->all();
        $course_name = auth()->user()->course_title;   

        if( auth()->user()->is_admin == 1 ) {

            $videos = Media::select('title','course_title','video','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'video')
                    ->orderBy('order', 'desc')
                    ->paginate(6);
        }else{
            $videos = Media::select('title','course_title','video','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'video')
                    ->where('course_title', $course_name)
                    ->orderBy('order', 'desc')
                    ->paginate(6);
        }
        return view('frontend.pages.video',compact('courses','videos'));
    }

    public function sheet()
    {
        $courses = Course::where('is_active',1)->pluck('course_name','id')->all();
        $course_name = auth()->user()->course_title;   

        if( auth()->user()->is_admin == 1 ) {

            $sheets = Media::select('title','course_title','pdf','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'pdf')
                    ->orderBy('order', 'desc')
                    ->get();
        }else{
            $sheets = Media::select('title','course_title','pdf','order','is_active')
                    ->where('is_active',1)
                    ->where('file_type', 'pdf')
                    ->where('course_title', $course_name)
                    ->orderBy('order', 'desc')
                    ->get();
        }
        return view('frontend.pages.pdf',compact('courses','sheets'));
    }

    public function gallery()
    {
        $galleryImage = Photo::where('is_active',1)->where('image_type','gallery')->get();

        return view('frontend.pages.gallery',compact('galleryImage'));
    }

}
