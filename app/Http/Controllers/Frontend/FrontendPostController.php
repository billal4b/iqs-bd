<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Blog;
Use App\Content;

class FrontendPostController extends Controller
{   
    public function blog()
    {
        $blogs = Blog::select('id','title','excerpt','content','url','date_time','image','is_active')
                ->where('is_active',1)
                ->orderBy('date_time', 'desc')
                ->paginate(12);

        return view('frontend.pages.blog',compact('blogs'));
    }

    public function blogDetails($url)
    { 
        $blogDetails = Blog::whereUrl($url)->first();
        $recentPosts = Blog::select('title','url','date_time','thumb','is_active')
            ->where('is_active',1)
            ->orderBy('date_time', 'desc')
            ->limit(5)
            ->get();   
               
        return view('frontend.pages.blog_details',compact('blogDetails','recentPosts'));
    }

    public function aboutUs()
    {
        $aboutUs = Content::where('is_active',1)
                ->where('section_name', 'About Us')
                ->orderBy('order', 'desc')
                ->get();
       
        return view('frontend.pages.about_us',compact('aboutUs'));
    }

    
    public function contactUs()
    {
        $contactUs = Content::where('is_active',1)
                    ->where('section_name', 'Contact Us')
                    ->first();

        return view('frontend.pages.contact_us',compact('contactUs'));
    }
  

}
