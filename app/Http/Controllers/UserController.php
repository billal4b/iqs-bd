<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Course;
use App\Mail\SendUserData;

class UserController extends Controller
{
    //protected $notifications;

    public function __construct()
    {
        $this->middleware('adminmiddleware');        
        //$notifications = DB::table('users')->count();        
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    { 
        $users = User::where('status','!=','cancel')->orderBy('id', 'desc')->paginate(25);
        return view('backend.users.index', compact('users'));
        //return view('backend.users.index');             
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $edit = User::findOrFail($id);   
        $courses = Course::where('is_active', 1)->get();
        return view('backend.users.edit', compact('edit','courses'));
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        
        $request->validate([            
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255'],
            'mobile'   => ['required'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $update = [
            'name'   => $request->name, 
            'email'  => $request->email,
            'mobile' => $request->mobile,
            'password'    => Hash::make($request->password),
            'course_title'=> $request->course_title,
            'blood_group' => $request->blood_group,
            'address'=> $request->address,
            'gender' => $request->gender,            
            'dob'    => $request->dob,
            'nid_no' => $request->nid_no,
            'status' => $request->status
        ];
        user::where('id',$id)->update($update);

        //send notification email to the user, if enabled
        $send_email = array(
            'name'           => trim($request['name']),
            'email'          => trim(strtolower($request['email'])),
            'password'       => $request['password'],
        );

        // if(!empty($send_email)){
        //     //print_r($send_email['email']); exit;
        //     $mail = $send_email['email'];
        //     Mail::to($mail)->send(new SendUserData ($send_email));

        // }
        
        return redirect('/admin/user')->with('success', 'User Information successfully updated');        
        //return back()->withStatus(__('Status successfully updated.'));
        //return Redirect::back()->with('success', 'Status is successfully updated');
    }
    
    public function countUser(){
        $users = User::count();
        
        return view('backend.layouts.dashboard', compact('users'));
    }
    
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->status == 'active' || $user->status == 'inactive') {
            $user->status = 'cancel';
        }
        $user->save();

        return redirect('/admin/user')->with('success', 'User has been deleted');
    }
}
