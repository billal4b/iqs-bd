<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

   protected $fillable = [
       'title', 'file_type','course_title', 'audio', 'video','pdf','order','is_active',
   ];
}
